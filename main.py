import numpy as np
import time


NUM_GENES = 200
MIN_GENE = -400
MAX_GENE = 400
POPULATION_LIMIT = 300
ITERATIONS = 280
MAX_BEST_CREATURES = 114
CHANCE_TO_MUTATE = 0.05
MUTATION_AMOUNT = 24
times_mated = 0
times_cloned = 0


def main():
    goal = create_random_creature()
    print('Goal: ', goal)
    population = create_random_pop()
    for n in range(0, ITERATIONS):
        evaluation = evaluate_pop(population, goal)
        best_creatures = get_best_creatures(population, evaluation)
        new_population = best_creatures
        for _ in range(0, POPULATION_LIMIT - len(new_population)):
            new_population.append(mate_random_creatures(population))
        population = new_population
        print('Evaluation iteration #', n, ': ', evaluation)
    # print('Last population: ', population)
    print('Goal: ', goal)
    best_creatures = get_best_creatures(population, evaluate_pop(population, goal))
    # print('Best creatures: ', best_creatures)
    print('Evaluation of best creatures: ', evaluate_pop(best_creatures, goal))
    print('Number of mates/clones in execution: ', times_mated, '/', times_cloned)


def create_random_creature():
    new_creature = []
    for _ in range(0, NUM_GENES):
        new_creature.append(np.random.randint(MIN_GENE, MAX_GENE))
    return new_creature


def create_random_pop():
    population = []
    for _ in range(0, POPULATION_LIMIT):
        population.append(create_random_creature())
    return population


def evaluate_pop(ppopulation, goal):
    evaluation = []
    for n in range(0, len(ppopulation)):
        evaluation.append(evaluate_creature(ppopulation[n], goal))
    return evaluation


def evaluate_creature(pcreature, pgoal):
    fitness = 0
    for n in range(0, NUM_GENES):
        desired = pgoal[n]
        gene = pcreature[n]
        step = 1
        if desired > gene:
            step = -1
        fitness += len(list(range(desired, gene, step)))
    return fitness


def get_best_creatures(ppopulation, pevaluation):
    best_creatures = []
    best_indexes = sorted(range(len(pevaluation)), key=lambda i: pevaluation[i])[:MAX_BEST_CREATURES]  # saco los indices de las evaluaciones que mas se aproximen a 0. 0 es "perfecto". La cantidad de indices es la cantidad de criaturas por sacar
    for n in best_indexes:
        best_creatures.append(ppopulation[n])
    return best_creatures


def mate_random_creatures(ppopulation):
    global times_cloned
    global times_mated
    parent1_index = np.random.randint(0, POPULATION_LIMIT)
    parent2_index = np.random.randint(0, POPULATION_LIMIT)
    while parent1_index == parent2_index:
        parent2_index = np.random.randint(0, POPULATION_LIMIT)
    if parent1_index == parent2_index:
        times_cloned += 1
    parent1 = ppopulation[parent1_index]
    parent2 = ppopulation[parent2_index]
    child = []
    for n in range(0, NUM_GENES):
        parent_gene = 0
        child_gene = 0
        if np.random.randint(1, 3) == 1:
            parent_gene = parent1[n]
        else:
            parent_gene = parent2[n]
        if np.random.random() < CHANCE_TO_MUTATE:
            child_gene = mutate(parent_gene)
        else:
            child_gene = parent_gene
        child.append(child_gene)
    times_mated += 1
    return child


def mutate(gene):
    return gene + np.random.randint(low=-MUTATION_AMOUNT, high=MUTATION_AMOUNT)


start_time = time.time()
main()
print("--- %s seconds ---" % (time.time() - start_time))